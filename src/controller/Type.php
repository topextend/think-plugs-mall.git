<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-12 18:35:45
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-19 17:01:58
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Type.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\controller;

use app\mall\model\MallType;
use app\mall\model\MallCate;
use think\admin\extend\DataExtend;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 商品类型管理
 * Class Type
 * @package app\mall\controller
 */
class Type extends Controller
{
    /**
     * 商品类型管理
     * @auth true
     * @menu true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        MallType::mQuery()->layTable(function () {
            $this->title = "商品类型管理";
        }, function (QueryHelper $query) {
            $query->like('type_name')->equal('status')->dateBetween('create_at');
        });
    }
    
    /**
     * 添加商品类型
     * @auth true
     */
    public function add()
    {
        MallType::mForm('form');
    }

    /**
     * 编辑商品类型
     * @auth true
     */
    public function edit()
    {
        MallType::mForm('form');
    }
    
    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            // 检查类型名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['type_name' => $data['type_name']];
                if (MallType::mk()->where($where)->count() > 0) {
                    $this->error("类型名称 {$data['type_name']} 已经存在，请使用其它类型名称！");
                }
            }
        }
    }

    /**
     * 修改商品类型状态
     * @auth true
     */
    public function state()
    {
        MallType::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除商品类型
     * @auth true
     */
    public function remove()
    {
        MallType::mDelete();
    }
}
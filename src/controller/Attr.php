<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-12 17:48:24
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-29 18:56:27
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Attr.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\controller;

use app\mall\model\MallAttr;
use think\admin\helper\QueryHelper;
use think\admin\Controller;

/**
 * 商品属性管理
 * Class Attr
 * @package app\mall\controller
 */
class Attr extends Controller
{
    /**
     * 商品属性管理
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        MallAttr::mQuery()->layTable(function () {
            $this->title = '属性列表管理';
            $this->type_id = $this->app->request->get('id');
        }, function (QueryHelper $query) {
            $query->like('name')->where(['type_id'=>trim(input('get.type_id'))])->equal('status')->dateBetween('create_at');
        });
    }

    /**
     * 添加商品属性
     * @auth true
     */
    public function add()
    {
        MallAttr::mForm('form');
    }

    /**
     * 编辑商品属性
     * @auth true
     */
    public function edit()
    {
        MallAttr::mForm('form');
    }
    
    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            if (isset($data['attr_value'])) $data['attr_value'] = rtrim(make_semiangle($data['attr_value']));
            if ($data['attr_type']=='0' && $data['attr_input_type']=='2') $this->error("单选属性无法不能为多维属性！");
            if ($data['attr_type']=='1' && $data['attr_input_type']!='1') $this->error("多选属性只能选择列表普通属性！");
            // 检查属性名称是否出现重复
            if (!isset($data['id'])) {
                $where = ['attr_name' => $data['attr_name'], 'type_id' => $data['type_id']];
                if (MallAttr::mk()->where($where)->count() > 0) {
                    $this->error("属性名称 {$data['attr_name']} 已经存在，请使用其它属性名称！");
                }
            }
        }
    }

    /**
     * 修改商品属性状态
     * @auth true
     */
    public function state()
    {
        MallAttr::mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除商品属性
     * @auth true
     */
    public function remove()
    {
        MallAttr::mDelete();
    }
}
<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-09 17:52:10
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-15 19:13:14
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : MallCate.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\model;

use think\admin\extend\DataExtend;
use think\admin\Model;

/**
 * 商品品牌模型
 * Class MallCate
 * @package app\mall\model
 */
class MallCate extends Model
{
    /**
     * 获取上级可用选项
     * @param int $max 最大级别
     * @param array $data 表单数据
     * @param array $parent 上级分类
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getParentData(int $max, array &$data, array $parent = []): array
    {
        $items = static::mk()->where(['deleted' => 0])->order('sort desc,id asc')->select()->toArray();
        $cates = DataExtend::arr2table(empty($parent) ? $items : array_merge([$parent], $items));
        if (isset($data['id'])) foreach ($cates as $cate) if ($cate['id'] === $data['id']) $data = $cate;
        foreach ($cates as $key => $cate) {
            $isSelf = isset($data['spt']) && isset($data['spc']) && $data['spt'] <= $cate['spt'] && $data['spc'] > 0;
            if ($cate['spt'] >= $max || $isSelf) unset($cates[$key]);
        }
        return $cates;
    }

    /**
     * 获取数据树
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function treeData(): array
    {
        $query = static::mk()->where(['status' => 1, 'deleted' => 0])->order('sort desc,id desc');
        return DataExtend::arr2tree($query->withoutField('sort,status,deleted,create_at')->select()->toArray());
    }

    /**
     * 获取列表数据
     * @param bool $simple 仅子级别
     * @return array
     */
    public static function treeTable(bool $simple = false): array
    {
        $query = static::mk()->where(['status' => 1, 'deleted' => 0])->order('sort desc,id asc');
        $cates = array_column(DataExtend::arr2table($query->column('id,pid,name', 'id')), null, 'id');
        foreach ($cates as $cate) isset($cates[$cate['pid']]) && $cates[$cate['id']]['parent'] =& $cates[$cate['pid']];
        foreach ($cates as $key => $cate) {
            $id = $cate['id'];
            $cates[$id]['ids'][] = $cate['id'];
            $cates[$id]['names'][] = $cate['name'];
            while (isset($cate['parent']) && ($cate = $cate['parent'])) {
                $cates[$id]['ids'][] = $cate['id'];
                $cates[$id]['names'][] = $cate['name'];
            }
            $cates[$id]['ids'] = array_reverse($cates[$id]['ids']);
            $cates[$id]['names'] = array_reverse($cates[$id]['names']);
            if (isset($pky) && $simple && in_array($cates[$pky]['name'], $cates[$id]['names'])) {
                unset($cates[$pky]);
            }
            $pky = $key;
        }
        foreach ($cates as &$cate) unset($cate['parent']);
        return array_values($cates);
    }

    /**
     * 获取列表数据
     * @param string $cats
     * @return string
     */
    public static function getSelectCates()
    {
        $cat = static::mk()->where(['pid' => 0])->column('id, name');
        foreach ($cat as $key => $value)
        {
            $value['child'] = static::mk()->where(['pid' => $cat[$key]['id']])->column('id, name');
            foreach ($value['child'] as $kk => $vv)
            {
                $vv['child'] = static::mk()->where(['pid' => $value['child'][$kk]['id']])->column('id, name');
                $value['child'][$kk] = $vv;
            }
            $cats['option'][$key] = $value;
        }
        return json_encode($cats, JSON_UNESCAPED_UNICODE);
    }
    
    /**
     * 商品发布获取已选定商品分类
     * @return string
     */
    public static function getSelectedCatPath(string $cat_id) : string
    {
        $cat_3 = static::mk()->where(['id' => $cat_id])->field('pid, name')->find();
        $cat_2 = static::mk()->where(['id' => $cat_3['pid']])->field('pid, name')->find();
        $cat_1 = static::mk()->where(['id' => $cat_2['pid']])->field('pid, name')->find();
        return $cat_1['name'] . "  >>  " . $cat_2['name'] ."  >>  " . $cat_3['name'];
    }
    
    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}
<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-12 17:50:09
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-29 16:55:10
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : MallAttr.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\mall\model;

use think\admin\Model;

/**
 * 商品属性模型
 * Class MallAttr
 * @package app\mall\model
 */
class MallAttr extends Model
{
    /**
     * 通过类型ID获取相应属性
     * @var string value
     * @return string
     */
    public static function getAttrList(string $type_id)
    {
        $attrs = static::mk()->where(['type_id'=>$type_id,'status'=>'1'])->order('sort desc,id asc')->select()->toArray();
        foreach ($attrs as $key => $attr) {
            if ($attr['attr_input_type'] == '1')
            {
                $attr_value = [];
                foreach (explode(',', $attr['attr_value']) as $k => $v)
                {
                    $attr_value[$k]['name']  = $v;
                    $attr_value[$k]['value'] = $k;
                }
                $attrs[$key]['attr_value'] = json_encode($attr_value);
            }
            if ($attr['attr_input_type'] == '2')
            {
                // $attrs[$key]['attr_value'] = explode(';', $attr['attr_value']);
                // foreach ($attrs[$key]['attr_value'] as $k => $vo) 
                // {
                //     $attrs[$key]['attr_value'][$k] = explode(',', $vo);
                // }
            }
        }
        return $attrs;
    }
}